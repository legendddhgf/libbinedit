#include <stdlib.h>
#include <stdio.h>
#include <string>
#include <libbinedit.hpp>

int main(int argc, char **argv) {
  if (argc < 2) {
    fprintf(stderr, "Demo requires one argument to run\n");
    exit(1);
  }
  printf("Running dynamic demo of libbinedit\n");
  printf("Running as %s %s\n", argv[0], argv[1]);
  testing();
  return 0;
}
