MAIN = libbinedit
INSTALL_LIB_DIR = /usr/local/lib
INSTALL_INC_DIR = /usr/include
OUT = ${MAIN:%=%.so}
MODULE =
SRC = ${MAIN:%=%.cpp} ${MODULE:%=%.cpp}
HDR = ${SRC:%.cpp=%.hpp}
OBJ = ${SRC:%.cpp=%.o}
COMPILE = g++ -c -Wall -pedantic -std=c++17
SOLINK = g++ -shared -o
LINK = g++ -o

all: ${OUT}

install: ${OUT}
	cp -f ${OUT} ${INSTALL_LIB_DIR}
	cp -f ${HDR} ${INSTALL_INC_DIR}

${OUT}: obj
	${SOLINK} ${OUT} ${OBJ}

obj: ${SRC} ${HDR}
	${COMPILE} -fPIC ${SRC}

clean:
	rm -f ${OBJ} ${OUT}

uninstall:
	rm -f ${INSTALL_LIB_DIR}/${OUT}
	rm -f ${INSTALL_INC_DIR}/${HDR}

# attempt to use the present working directory
demo_default: all
	${COMPILE} -I. demo_default.cpp
	${LINK} demo_default -L. demo_default.o -lbinedit
	@echo "\n=====ATTEMPT DEMO=====\n"
	./demo_default test123
	@echo "\n=====END DEMO=====\n"
	rm -f demo_default demo_default.o

# attempt to use the install directory
demo_dynamic:
	${COMPILE} -I${INSTALL_INC_DIR} demo_dynamic.cpp
	${LINK} demo_dynamic -L${INSTALL_LIB_DIR} demo_dynamic.o -lbinedit
	@echo "\n=====ATTEMPT DEMO=====\n"
	./demo_dynamic test123
	@echo "\n=====END DEMO=====\n"
	rm -f demo_dynamic demo_dynamic.o

.PHONY: all install obj clean uninstall demo_default demo_install
